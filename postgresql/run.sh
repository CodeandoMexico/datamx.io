#!/bin/bash

__dir=$(dirname $(realpath $0))
ENV_FILE=$__dir/../environment

podman run \
    --env-file=$ENV_FILE \
    --volume=datamx_database:/var/lib/postgresql/data \
    --name=datamx-postgresql \
    --rm \
    --pod datamx-pod \
    datamx-postgresql
