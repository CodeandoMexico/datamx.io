from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the relevant file
with open(path.join(here, 'README.md')) as f:
    long_description = f.read()

setup(
    name='customschemas',

    version='1.0',

    description='Some schemas for tweaking in projects',
    long_description=long_description,
    long_description_content_type="text/markdown",

    # The project's main homepage.
    url='',

    # Author details
    author='',
    author_email='',

    # Choose your license
    license='AGPL',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Programming Language :: Python :: 3.8',
    ],

    # What does your project relate to?
    keywords='''CKAN theme''',

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),

    install_requires=[
    ],

    # If there are data files included in your packages that need to be
    # installed, specify them here.  If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
    include_package_data=True,
    package_data={
        'schemas': ['dataset.yml'],
    },
)
