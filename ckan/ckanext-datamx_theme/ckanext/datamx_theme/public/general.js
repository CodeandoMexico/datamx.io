document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('nav-toggle').addEventListener('click', () => {
    document.querySelector('.dmxnav__body').classList.toggle('visible');
  });
});
