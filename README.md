# datamx.io

Repositorio con las fuentes a partir de las cuales se construyen los
contenedores que utiliza datamx.io

## Desarrollo

### Nota sobre docker y podman

El proyecto fue construido usando podman en lugar de docker durante todo el
proceso. Habiendo dicho eso, es posible usad docker para construir las imágenes
y levantarlo en local. En muchos de los comandos basta con reemplazar `podman`
con `docker`.

### Clonación del proyecto

Este proyecto depende de versiones específicas de CKAN y de ciertos plugins que
están incluídas como subrepositorios. Por lo tanto la forma correcta de clonar
este proyecto es:

    git clone --recurse-submodules https://gitlab.com/codeandomexico/datamx.io.git

Si ya estaba clonado se pueden inicializar los subrepos con esto:

    git submodule init
    git submodule update

### Construicción de las imágenes

Cada servicio aquí usado tiene su propio `build.sh` y `Containerfile` excepto
redis, del cual se usa la imagen oficial. Para constriur cada uno basta con
hacer:

    ./postgresql/build.sh
    ./solr/build.sh
    ./ckan/build.sh

En el caso de redis basta con bajar la imagen:

    podman pull docker.io/redis:6.2

### Configuración

La ejecución de los diferentes servicios requiere un único archivo de
configuración que concentra todos los parámetros necesarios para su
inicialización. Después de la primera ejecución es posible continuar la
configuración haciendo uso de los archivos específicos de cada servicio.

Para comenzar hay que copiar el archivo `example.env` en un archivo
`environment` esperado por los diferentes scripts de ejecución y ajustar el
contenido del mismo.

**Advertencia** Por la forma en que algunas variables son reemplazadas en el
archivo de configuración existe cierta sensibilidad a la presencia del caracter
`'/'` en los valores, así que es mejor evitarlo (por ejemplo en contraseñas).

### Levantar los servicios (específico de podman)

Primero hay que crear (o asegurar que exista) el pod. Este es el "host" que une
a todos los servicios para que se puedan localizar entre sí en localhost:

    ./pod/run.sh

Luego se pueden levantar los diferentes contenedores que integran el pod:

    ./postgresql/run.sh
    ./redis/run.sh
    ./solr/run.sh

Finalmente se puede levantar el servicio de ckan y datapusher:

    ./ckan/run.sh
    ./datapusher/run.sh

### Ejecutar comandos de CKAN

Hay en la raíz del proyecto un script ejecutable `ckan.sh` que simplifica la
ejecución de comandos dentro del contenedor.

Por ejemplo para crear un usuario sysadmin:

    ./ckan.sh sysadmin add <username> email=<email> name=<username>

### Creación de una extensión

Para crear una nueva extensión hay que crear primero la carpeta local en la que
va a vivir:

    mkdir tmp

luego modificar temporalmente el archivo `ckan.sh` para montar un volumen en esa
carpeta

    --volume=$__dir/tmp:/opt/ckan/tmp:rw \

y generarla:

    ./ckan.sh generate extension -o /opt/ckan/tmp

Ahora necesitarás cambiar la dueña de la nueva extensión a tu propio usuario:

    sudo chown -R tu:tu tmp

y mover la extensión generada a su ubicación junto a las demás:

    mv tmp/ckanext-whatever ckan/

En este punto lo que sigue es editar el Containerfile para integrar la nueva
extensión como se hace con las demás y modificar el `ckan.ini` almacenado en el
volumen de configuración para cargarla. Será necesario reconstruir el
contenedor.

Para el desarrollo de la extensión lo ideal es montar la carpeta de la misma en
nuestra computadora como un volumen dentro del contenedor en la ubicación a la
que se copió (seguramente dentro de `/opt/ckan/plugins`) como sigue:

    --volume=$__dir/ckanext-filterprivate/ckanext:/opt/ckan/plugins/ckanext-filterprivate/ckanext:rw \

### Recargar el servicio de CKAN

Para recargar el contenedor de CKAN sin reiniciarlo y volverlo a iniciar a
manita:

    kill -s HUP $(podman inspect ckan | jq .[0].State.Pid)

requere de [jq](https://stedolan.github.io/jq/). Esto está puesto ya en un
script:

    ./scripts/reload.sh

### Creación de los archivos de systemd

Levantar todos los contenedores como arriba y luego correr:

    podman generate systemd --files --name --new --pod-prefix= --container-prefix= datamx-pod

los archivos quedarán en la carpeta actual. Será necesario ajustar las rutas a
archivos de configuración y entornos. Una vez movidos al servidor de producción
y atualizadas las unidades con `systemctl --user daemon-reload` es posible
iniciar todos los servicios con

    systemctl --user start ckan-pod

así mismo es posible detenerlos todos con

    systemctl --user stop ckan-pod

## Producción

Monitorear los servicios que componen esta instalación de CKAN es muy sencillo.
Cada parte de la infraestructura corre como un servicio tradicional de `systemd`
y se monitorean y administran acorde. La lista de servicios es la siguiente:

* datamx-pod.service
* datamx-postgresql.service
* datamx.service
* datamx-redis.service
* datamx-solr.service
* datamx-datapusher.service
* ckan-notifications.service
* ckan-notifications.timer

Como son servicios ordinarios de systemd se pueden monitorear usando el journal
del sistema:

    journalctl [-f] --unit datamx.service

O reiniciar:

    systemctl restart datamx.service

La configuración de CKAN se encuentra administrada en el [repositorio de
infraestructura de codeandomexico](https://gitlab.com/codeandomexico/infra) y se
publica usando [saltstack](https://saltproject.io/).
