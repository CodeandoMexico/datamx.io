#!/bin/bash

__dir=$(dirname $(realpath $0))
ENV_FILE=$__dir/../environment

podman run \
    --env-file=$ENV_FILE \
    --volume=datamx_solr_data:/opt/solr/server/solr/ckan/data \
    --name=datamx-solr \
    --rm \
    --pod datamx-pod \
    datamx-solr
