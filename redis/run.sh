#!/bin/bash

podman run \
    --name=datamx-redis \
    --rm \
    --pod datamx-pod \
    redis:6.2
