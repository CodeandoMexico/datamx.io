#!/bin/bash

__dir=$(dirname $(realpath $0))

mkdir -p $__dir/../storage/config/ckan
mkdir -p $__dir/../storage/config/datapusher
mkdir -p $__dir/../storage/data
