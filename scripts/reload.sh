#!/bin/bash

# Reloads the current instance of the ckan container.

kill -s HUP $(podman inspect datamx | jq .[0].State.Pid)
