set -e

if [ -z $1 ]; then
    echo 'First argument must be a filename where every line is the name of a
group to be created'
    exit 1;
fi

if [ ! -e $1 ]; then
    echo "file '$1' does not exist"
    exit 1;
fi

if [ -z $CKAN_API_TOKEN ]; then
    echo "Pleas input api token: "
    read token
else
    token=$CKAN_API_TOKEN
fi

if [ -z $CKAN_HOST ]; then
    host=http://localhost:5000
else
    host=$CKAN_HOST
fi

for name in `cat $1`; do
    slug=${name,,}
    slug=${slug// /-}

    echo "Creating group '$name' with slug '$slug'"

    curl \
        -X POST \
        -s \
        -H "Authorization: $token" \
        -d "{\"name\": \"$slug\", \"title\": \"$name\"}" \
        $host/api/3/action/group_create | jq .success
done
